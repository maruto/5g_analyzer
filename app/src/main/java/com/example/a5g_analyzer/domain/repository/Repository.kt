package com.example.a5g_analyzer.domain.repository

import com.example.a5g_analyzer.domain.model.CellDataItemModel

interface Repository {

	fun uploadDataToServer(cellData: ArrayList<CellDataItemModel>) : Boolean

	fun getNewCellData() : ArrayList<CellDataItemModel>
}