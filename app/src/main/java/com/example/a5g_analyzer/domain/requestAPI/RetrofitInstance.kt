package com.example.a5g_analyzer.domain.requestAPI

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitInstance {
	private val retrofit by lazy {

		Retrofit.Builder()
			.baseUrl("https://webhook.site")
			.addConverterFactory(GsonConverterFactory.create())
			.build()
	}

	val api: RequestAPI by lazy {
		retrofit.create(RequestAPI::class.java)
	}
}