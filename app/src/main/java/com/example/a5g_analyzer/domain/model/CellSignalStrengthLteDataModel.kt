package com.example.a5g_analyzer.domain.model

data class CellSignalStrengthLteDataModel(
    val asuLevel: Int,
    val cqi: Int,
    val cqiTableIndex: Int,
    val dbm: Int,
    val level: Int,
    val rsrp: Int,
    val rsrq: Int,
    val rssi: Int,
    val rssnr: Int,
    val ta: Int
)