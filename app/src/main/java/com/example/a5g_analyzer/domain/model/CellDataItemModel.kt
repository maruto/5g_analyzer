package com.example.a5g_analyzer.domain.model

data class CellDataItemModel(
	val CellIdentityLteDataModel: CellIdentityLteDataModel,
	val CellSignalStrengthLteDataModel: CellSignalStrengthLteDataModel,
	val mCellConnectionStatus: Int,
	val mRegistered: Boolean,
	val mTimeStamp: Long
)
