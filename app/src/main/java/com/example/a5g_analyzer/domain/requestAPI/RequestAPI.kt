package com.example.a5g_analyzer.domain.requestAPI

import com.example.a5g_analyzer.domain.model.CellDataItemModel
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface RequestAPI {

	@POST("/434ad872-c9c9-4cfc-89fa-8b0a5fc6681f")
	fun uploadData(@Body cellData: ArrayList<CellDataItemModel>): Call<ResponseBody>
}