package com.example.a5g_analyzer.domain.model

data class CellIdentityLteDataModel(
    val ci: Int,
    val earfcn: Int,
    val mAdditionalPlmns: List<String>,
    val mAlphaLong: String,
    val mAlphaShort: String,
    val mBandwidth: Int,
    val mMcc: Int,
    val mMnc: Int,
    val mNetworkOperator: String,
    val pci: Int,
    val tac: Int
)