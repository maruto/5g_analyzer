package com.example.a5g_analyzer

import android.Manifest
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.work.Constraints
import androidx.work.NetworkType
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {

	private val permissionsRequestLauncher = registerForActivityResult(
		ActivityResultContracts.RequestMultiplePermissions(),
		::onGotPermissionsResult
	)

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)
		permissionsRequestLauncher.launch (
			arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION,  Manifest.permission.ACCESS_FINE_LOCATION)
		)


	}

	private fun onGotPermissionsResult(grantResults: Map<String, Boolean>) {
		if(grantResults.entries.all {it.value}) {
			val constraints = Constraints.Builder()
				.setRequiresBatteryNotLow(true)
				.setRequiredNetworkType(NetworkType.CONNECTED)
				.build()
			val  workRequest =
				PeriodicWorkRequestBuilder<DataSendWorker>(15, TimeUnit.MINUTES, 10, TimeUnit.MINUTES)
					.setConstraints(constraints)
					.build()
			WorkManager.getInstance(this).enqueue(workRequest)
			WorkManager.getInstance(this)
				.getWorkInfoByIdLiveData(workRequest.id)
				.observe(this) {
					Log.d("workRequest", "onChanged: " + it.state);
				}
		} else {
			Toast.makeText(this, getString(R.string.toastPermissionDenied), Toast.LENGTH_SHORT).show()
		}
	}
}
