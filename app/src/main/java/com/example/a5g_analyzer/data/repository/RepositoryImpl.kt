package com.example.a5g_analyzer.data.repository

import android.content.Context
import android.os.Build
import android.telephony.CellIdentityLte
import android.telephony.CellInfo
import android.telephony.CellSignalStrengthLte
import android.telephony.TelephonyManager
import android.util.Log
import com.example.a5g_analyzer.domain.model.CellDataItemModel
import com.example.a5g_analyzer.domain.model.CellIdentityLteDataModel
import com.example.a5g_analyzer.domain.model.CellSignalStrengthLteDataModel
import com.example.a5g_analyzer.domain.repository.Repository
import com.example.a5g_analyzer.domain.requestAPI.RetrofitInstance

import java.lang.Exception

class RepositoryImpl(
	private val context: Context,
) : Repository {
	private val TAG : String = "5G_ANALYZER"
	private var telephonyManager: TelephonyManager = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

	override fun uploadDataToServer(cellData: ArrayList<CellDataItemModel>) : Boolean {

		val response = RetrofitInstance.api.uploadData(cellData).execute()
		return response.isSuccessful;
	}

	override fun getNewCellData() : ArrayList<CellDataItemModel> {
		return createModel( telephonyManager.allCellInfo)
	}

	private fun createModel(cellInfoList: List<CellInfo>) : ArrayList<CellDataItemModel> {
		val cellDataModel : ArrayList<CellDataItemModel> = ArrayList()
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S  ) {
			for(cellInfo : CellInfo in cellInfoList) {
				try {
					cellDataModel.add(
						CellDataItemModel(
							createCellIdentityLteDataModel(cellInfo.cellIdentity as CellIdentityLte),
							createCellSignalStrengthLteDataModel(cellInfo.cellSignalStrength as CellSignalStrengthLte),
							cellInfo.cellConnectionStatus,
							cellInfo.isRegistered,
							cellInfo.timestampMillis
						)
					)
				} catch (e : ClassCastException) {
					Log.d(TAG + "_ClassCastEx", cellInfo.toString())
				} catch (e : Exception) {
					Log.d(TAG + "_CreateExc", e.message.toString())
				}
			}
		} else {
			 throw Exception("Не удалось создать createCellIdentityLteDataModel, не соответсвует уровень SDK")
		}
		return cellDataModel;
	}

	private fun createCellIdentityLteDataModel(cellIdentityLte: CellIdentityLte) : CellIdentityLteDataModel {

		return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
			 CellIdentityLteDataModel(
				 cellIdentityLte.ci,
				 cellIdentityLte.earfcn,
				 cellIdentityLte.additionalPlmns.toList(),
				 cellIdentityLte.operatorAlphaLong.toString(),
				 cellIdentityLte.operatorAlphaShort.toString(),
				 cellIdentityLte.bandwidth,
				 cellIdentityLte.mccString?.toInt() ?: 0,
				 cellIdentityLte.mncString?.toInt() ?: 0,
				 cellIdentityLte.mobileNetworkOperator.toString(),
				 cellIdentityLte.pci,
				 cellIdentityLte.tac
			)
		} else {
			throw Exception("Не удалось создать createCellIdentityLteDataModel, не соответсвует уровень SDK")
		}
	}

	private fun createCellSignalStrengthLteDataModel(cellSignalStrengthLte: CellSignalStrengthLte) : CellSignalStrengthLteDataModel {
		return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
			CellSignalStrengthLteDataModel(
				cellSignalStrengthLte.asuLevel,
				cellSignalStrengthLte.cqi ,
				cellSignalStrengthLte.cqiTableIndex,
				cellSignalStrengthLte.dbm,
				cellSignalStrengthLte.level,
				cellSignalStrengthLte.rsrp,
				cellSignalStrengthLte.rsrq,
				cellSignalStrengthLte.rssi,
				cellSignalStrengthLte.rssnr,
				cellSignalStrengthLte.timingAdvance
			)
		} else {
			throw Exception("Не удалось создать createCellIdentityLteDataModel, не соответсвует уровень SDK")
		}
	}
}