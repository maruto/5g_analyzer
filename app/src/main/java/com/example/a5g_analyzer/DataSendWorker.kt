package com.example.a5g_analyzer

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.example.a5g_analyzer.data.repository.RepositoryImpl
import com.example.a5g_analyzer.domain.repository.Repository

class DataSendWorker(context: Context, workerParams: WorkerParameters) : Worker(context, workerParams) {

	private  var repo : Repository = RepositoryImpl(applicationContext)

	override fun doWork(): Result {

		return try {
			if (repo.uploadDataToServer(repo.getNewCellData())) {
				Result.success()
			} else {
				Result.failure()
			}
		} catch (e: Exception) {
			Result.failure()
		}
	}
}